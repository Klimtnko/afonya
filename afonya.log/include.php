<?php

use \Afonya\NewsLog as NewsLogClass,
    \Afonya\HlBlock as HlBlockClass,
    \Afonya\Options as OptionsClass,
    \Afonya\Sites as SitesClass,
    \Afonya\Users as UsersClass;

CModule::AddAutoloadClasses(
    'afonya.log',
    [
        NewsLogClass::class => 'lib/NewsLog.php',
        HlBlockClass::class => 'lib/HlBlock.php',
        OptionsClass::class => 'lib/Options.php',
        SitesClass::class => 'lib/Sites.php',
        UsersClass::class => 'lib/Users.php',
    ]
);
