<?php
namespace Afonya;

class Users
{
    public function __construct()
    {
        $this->FIELDS = array(
            "FIELDS" => array(
                "ID",
                "NAME",
                "LAST_NAME",
                "EMAIL",
            )
        );
    }

    /**
     * @param array $arUsers
     * @return array
    */
    public function getList(array $arUsers)
    {
        $arResult = [];

        $rsUsers = \CUser::GetList(
            $by = "id",
            $order = "asc",
            array(
                "ID" => $this->getUserIdString($arUsers)
            ),
            $this->FIELDS
        );

        while ($arUser = $rsUsers->Fetch()) {
            $arResult[$arUser["ID"]] = $arUser["LAST_NAME"] . " " . $arUser["NAME"] . " (" . $arUser["EMAIL"] . ")";
        }

        return $arResult;
    }

     /**
     * @param array $arUsers
     * @return string
    */
    private function getUserIdString(array $arUsers): string
    {   
        $arResult = "";

        foreach ($arUsers as $user => $item) {
            $arResult = $arResult ? $arResult . " | " . $user : $user;
        }

        return $arResult;
    }
}