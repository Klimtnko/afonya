<?php
namespace Afonya;

class Sites
{
    /**
     * @return array
    */
    public function getList(array $arFilter = []): array
    {
        $arResult = [];
        $rsSites = \CSite::GetList($by="sort", $order="desc", $arFilter);

        while ($arSite = $rsSites->Fetch()) {
            $arResult[] = $arSite["LID"];
        }

        return $arResult;
    }
}