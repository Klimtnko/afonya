<?php
namespace Afonya;

use Bitrix\Highloadblock as HL;

class HlBlock
{
    private const ENTITY = "Stats";

    public function __construct()
    {
        $this->entity = $this->getEntity();
    }

    /**
     * @param array $arParams
     * @return
    */
    public function add(array $arParams)
    {
        return $this->entity::add($arParams);
    }

    /**
     * @param array $arParams
     * @return
    */
    public function update(array $arParams)
    {
        return $this->entity::update($arParams["ID"], $arParams);
    }

    /**
     * @param $arParams
     * @return array
    */
    public function getList(array $arParams = []): array
    {
        $arData = [];
        $rsResult = $this->entity::getList($arParams);

        while ($arResult = $rsResult->Fetch()) {
            $arData[] = $arResult;
        }

        return $arData;
    }

    /**
     * @return
    */
    private function getEntity()
    {
        if (\CModule::IncludeModule('highloadblock')) {
            $rs = HL\HighloadBlockTable::getList(
                array(
                    'filter' => array(
                        'NAME' => self::ENTITY
                    )
                )
            );

            if ($hlblock = $rs->fetch()) {
                $entity = HL\HighloadBlockTable::compileEntity($hlblock);
                return $entity->getDataClass();
            }
        }

        return false;
    }
}